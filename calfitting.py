#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 15:52:58 2017

@author: py13je
"""

#Polynomial fitting
import numpy as np
import matplotlib.pyplot as plt
#Data importing
data=np.genfromtxt(
                   "calibration.csv",
                   skip_header=1,
                   delimiter=",",)
print data
fit1=np.polyfit(data[0:,6],data[0:,4],1)

#Generation of x data for fitting
x=np.arange(0,3000,1000)
#Fitting
fitting1=np.poly1d(fit1)

y1=fitting1(x)

#Plotting
plt.plot(x,y1,label="$1^{st}$ order")

plt.scatter(data[0:,4],data[0:,6],marker='s',label="Points")
#plt.ylim(0,1)
#plt.xlim(0,1e7)
plt.xlabel("DEC (deg)")
plt.ylabel("Y (pixels)")
plt.title("Polynomial fitting for Calibration - Y Axis")
plt.legend(prop={'size':8})
plt.savefig("Data/Graphs/calfittingY.png",dpi=300)