\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {paragraph}{}{1}{section*.2}
\contentsline {paragraph}{}{1}{section*.3}
\contentsline {section}{\numberline {2}Stellar Clusters}{1}{section.2}
\contentsline {paragraph}{}{1}{section*.4}
\contentsline {paragraph}{}{1}{section*.5}
\contentsline {paragraph}{}{1}{section*.6}
\contentsline {subsection}{\numberline {2.1}NGC 2264}{2}{subsection.2.1}
\contentsline {paragraph}{}{2}{section*.7}
\contentsline {paragraph}{}{2}{section*.8}
\contentsline {section}{\numberline {3}Stellar Ageing Techniques}{2}{section.3}
\contentsline {paragraph}{}{2}{section*.9}
\contentsline {paragraph}{}{2}{section*.10}
\contentsline {subsection}{\numberline {3.1}Lithium Depletion Boundary}{2}{subsection.3.1}
\contentsline {paragraph}{}{2}{section*.11}
\contentsline {paragraph}{}{2}{section*.12}
\contentsline {paragraph}{}{2}{section*.13}
\contentsline {paragraph}{}{2}{section*.14}
\contentsline {subsection}{\numberline {3.2}Kinematic Ageing}{2}{subsection.3.2}
\contentsline {paragraph}{}{2}{section*.16}
\contentsline {paragraph}{}{3}{section*.17}
\contentsline {subsection}{\numberline {3.3}Lithium Abundance}{3}{subsection.3.3}
\contentsline {paragraph}{}{3}{section*.18}
\contentsline {paragraph}{}{3}{section*.19}
\contentsline {paragraph}{}{3}{section*.21}
\contentsline {subsection}{\numberline {3.4}HR Ageing}{3}{subsection.3.4}
\contentsline {paragraph}{}{3}{section*.22}
\contentsline {paragraph}{}{3}{section*.23}
\contentsline {section}{\numberline {4}MYStIX Survey}{3}{section.4}
\contentsline {paragraph}{}{3}{section*.24}
\contentsline {subsection}{\numberline {4.1}Infrared Excess Data From MIRES}{3}{subsection.4.1}
\contentsline {paragraph}{}{3}{section*.25}
\contentsline {paragraph}{}{4}{section*.27}
\contentsline {paragraph}{}{4}{section*.28}
\contentsline {subsection}{\numberline {4.2}X-Ray Data From MOXC}{4}{subsection.4.2}
\contentsline {paragraph}{}{4}{section*.30}
\contentsline {paragraph}{}{4}{section*.31}
\contentsline {section}{\numberline {5}Clustering Algorithms}{4}{section.5}
\contentsline {paragraph}{}{4}{section*.32}
\contentsline {paragraph}{}{4}{section*.33}
\contentsline {subsection}{\numberline {5.1}Previous Project}{4}{subsection.5.1}
\contentsline {paragraph}{}{4}{section*.34}
\contentsline {paragraph}{}{4}{section*.35}
\contentsline {subsection}{\numberline {5.2}Subclustering by MYStIX}{5}{subsection.5.2}
\contentsline {paragraph}{}{5}{section*.36}
\contentsline {paragraph}{}{5}{section*.37}
\contentsline {section}{\numberline {6}Potential Improvements For Next Years Project - The Gaia Survey}{5}{section.6}
\contentsline {paragraph}{}{5}{section*.39}
\contentsline {paragraph}{}{5}{section*.41}
\contentsline {paragraph}{}{6}{section*.43}
\contentsline {section}{\numberline {7}Discussion \& Conclusion}{6}{section.7}
\contentsline {paragraph}{}{6}{section*.45}
\contentsline {paragraph}{}{6}{section*.46}
