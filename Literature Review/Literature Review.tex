\documentclass[10pt,a4paper]{article}
\usepackage[width=15.92cm, height=24.62cm]{geometry}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{pdfpages}
\usepackage{url}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{float}
\usepackage[justification=centering,labelfont=it,textfont=it]{caption}
\usepackage{color}


\bibliographystyle{apalike}  

\hypersetup{
    colorlinks=true,
    linkcolor=red,
    filecolor=magenta,      
    urlcolor=red,
    citecolor=red,
}
%opening
\title{Stellar Clusters Project\\Literature Review}
\author{Joseph Eatson}
\date{November 2016}

\begin{document}

\maketitle
\tableofcontents
\begin{multicols}{2}
\section{Introduction}
\paragraph{}
This projects aim is to identify substructure and classify properties of young stellar clusters, and improve on the attempts to identify sub-clusters made by previous masters students.

\paragraph{}
\cite{BoyerIR} and \cite{GortonIR} have done similar projects, however this year a reduced number of candidates will be observed, and instead ageing techniques such as lithium abundance and HR fitting will be used to further refine substructure boundaries and properties.

\section{Stellar Clusters}
\paragraph{}
The previous projects surveyed multiple clusters, in this project however, work will be prioritised on a single cluster, NGC 2264, with others being surveyed if time permits it.

\paragraph{}
Clusters are large conglomerations of stars, which have formed at roughly the same time \cite{zeilikintroductory}. As extremely large molecular clouds collapse, they fracture into multiple smaller, unstable clouds, which leads to further fracturing and the formation of stars. This fracturing leads to slightly different ages of stars, as some molecular clouds are jeans unstable before others. This leads to the presence of substructure (namely subclusters) within the cluster itself.


\paragraph{}
The study of subclustering is important as it allows us to expand on our understanding of the evolution of stars, as they are the same age, and of similar compositions, but different masses, we can infer how initial conditions cause changes in the morphology of stars over time \cite{schmeja2006evolving}. This is more precise than observing changes over the entire cluster, which may have age variance in the order of hundreds of thousands to millions of years \cite{kuhn2015spatial}
\footnote{$\sigma$ of the aged subclusters in table 1 was found, for large clusters with $>4$ subclusters, an average deviation of $6.9z\times 10^5$ years was found, with a maximum deviation of $1.1\times 10^6$ and minimum of $3.9\times 10^5$ years.}. Being able to overcome this age variance is exceptionally useful for YSOs, particularly MYSOs, which have particularly rapid evolution timescales, and are poorly understood.

\subsection{NGC 2264}
\paragraph{}
The cluster observed within the project is NGC 2264, a young open star cluster, consisting primarily of T-Tauri stars and massive young stellar objects (MYSOs). \cite{povich2013mystix} states that there are 281 YSO candidates (YSOc) out of 1330 IR excess sources in the full MIRES catalogue.


\paragraph{}
As the cluster is fairly young, lithium is still abundant, which allows for ageing of stars using the semi-fundamental Lithium depletion boundary and the empirical Lithium abundance methods, which are detailed in \cite{soderblom2014ages}. This ties into the project, as stars in the same sub-cluster typically form in the same molecular clouds \cite{clarke2000protostars}, meaning they have approximately the same ages, hence ageing techniques are useful in determining the regions and boundaries of sub-clusters.

\section{Stellar Ageing Techniques}
\paragraph{}
According to \cite{soderblom2014ages}, ageing techniques can be sorted into the categories of fundamental, semi-fundamental and empirical techniques. Fundamental techniques rely solely on well-understood physics, and do not require a theoretical model, for instance, radiometric dating of material in the stellar system, such as isotope levels in meteorites. However this is not feasible for the ageing of stars within clusters, as radiometric data is obviously not attainable for stars hundreds of parsecs away.

\paragraph{}
Semi-fundamental techniques are important as they are particularly insensitive to initial conditions within a theoretical model and rely heavily on fundamental physics. Meanwhile empirical techniques are highly model sensitive, and hence can potentially be unreliable, however these are potentially more versatile, as they do not need extremely precise input data, such as 3D velocity data, or particularly precise spectrographic data.
\subsection{Lithium Depletion Boundary}
\paragraph{}
Arguably the most precise method of acquiring an absolute age without heavy reliance on a theoretical model is the Lithium Depletion Boundary method. Beyond $3\times 10^6K$ lithium destroyed in stars via the P-P chain reaction \cite{ushomirsky1998light}, this vigorous burning completely depletes observable lithium within $5\%$ of the lifespan of the star\cite{soderblom2014ages}, and creates a sharp boundary between less luminous, lithium rich and more luminous, lithium depleted stars, providing a fairly accurate metric for ageing.

\paragraph{}
Due to this rapid destruction of lithium however, there is only a specific window where the LDB method can be used, and does not extend past the zero age main sequence (ZAMS). In addition, it requires a spectroscopic sample of fairly good resolution in order to calculate the line width (and hence abundance), this will not be available for all stars, especially embedded sources.

\paragraph{}
Figure \ref{lithHR} displays compares stars aged via the lithium abundance method, the HR fitting method, as well as a density plot of stars present in the cluster, according to the MIRES survey\cite{povich2013mystix}.

\paragraph{}
Whilst the lithium abundance method (section \ref{lith}) is not to be confused with the lithium depletion boundary method, it has the same spectroscopic requirement. As can be seen lithium spectra have not been analysed for stars that are in denser sections of the cluster. 

\begin{figure}[H]
\centering
\includegraphics[width=\linewidth]{images/lithcomp2}
\caption{A composite of lithium abundance aged stars, HR aged stars and cluster candidate stars}
\label{lithHR}
\end{figure}

\subsection{Kinematic Ageing}
\paragraph{}
%This section is a bit choppy
Kinematic ageing relies on having the proper motion or 3D velocity data of the star, through this, the age is calculated by estimating the smallest volume the cluster sector occupies, and calculating the time taken to reach its current configuration \cite{brown1997kinematic}. This method does not rely on any assumptions about the internal composition of young stars, making it fairly model insensitive.
\paragraph{}
However, kinematic ageing will not used in this project, as without 3D motion data, it is fairly unreliable, whilst Gaia data may prove vital to the ageing of stars using this method, the current TGAS (Tycho-Gaia Astrometric Survey) data is insuffient for all but the closest clusters\footnote{Section \ref{tgas} has a more in depth review of potential use of the Gaia all-sky survey for 3D mapping of clusters, and the associated difficulties of utilising TGAS.}.


\subsection{Lithium Abundance}\label{lith}
\paragraph{}
The lithium abundance method uses theoretical models of the convective layers of young stars, comparing observed lithium emission line width to a theoretical model's estimates to provide ages for the stars. However, multiple models exist, and offer differing results, making this an empirical method of ageing. Additionally, as the properties of the convective layer and atmosphere of young stars is still largely unknown, there is still a great deal of imprecision.

\paragraph{}
\cite{lim2016constraint} has provided lithium abundance data for much of NGC 2264, however has recommended it solely for the use of relative ageing. However this is still useful for the identification of sub-clusters, as stars formed from the same gas cloud will have similar initial elemental abundances, and will be formed at roughly the same time, this correlation can be seen in figure \ref{limsurvey}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{images/lithtext2}
	\caption{A plot of the positions and abundances of a dense region of NGC 2264, from the \cite{lim2016constraint} dataset.}
	\label{limsurvey}
\end{figure}

\paragraph{}
The data from \cite{lim2016constraint}, as seen in figure \ref{lithHR} has some overlap with nearby HR aged sources from \cite{marinas2013near}, which could be used to find a relation between age and lithium abundance for this particular cluster, especially combined with other ageing data from other lithium abundance surveys, such as \cite{dahm2005t}, which is a spectroscopic survey of 490 T-Tauri stars within NGC 2264, with $H_\alpha$ and lithium spectra.

\subsection{HR Ageing}
\paragraph{}
Hertzsprung Russell ageing is perhaps one of the most versatile of the ageing methods, capable of getting absolute ages from dense regions of clusters, such as the data from FLAMINGOS study in \cite{marinas2013near}, which aged 54 sources from the dense southern region of NGC 2264. However it is entirely model dependent, for instance in \cite{marinas2013near} 3 models, \cite{clarke2000protostars}, \cite{baraffe1998evolutionary} and \cite{siess2000internet} were used in conjunction. Whilst this can account for systematic variance within the models, it is especially a problem for young stellar objects, 

\paragraph{}
The HR ageing method still requires spectroscopic data to correctly assess the stellar classification of the targets, which was done in the FLAMINGOS study through water bands for dim, M-class objects, and H-band line ratios for brighter stars. 

\section{MYStIX Survey}
\paragraph{}
The Massive Young Star-Forming Complex Study in Infrared and X-ray (MYStIX) is a study identifying member clusters within young open clusters \cite{feigelson2013overview}. Infrared excess is used to determine whether sources are embedded within a molecular cloud, whilst X-Ray radiation is emitted through two processes, accretion from circumstellar disks and shocks formed by interaction between molecular clouds and YSOs.
\subsection{Infrared Excess Data From MIRES}
\paragraph{}
The main catalog used for determining cluster members is the MYStIX InfraRed-Excess Source catalog (MIRES), which was built by \cite{povich2013mystix}. As the name states, it uses infrared excess to determine whether or not a target star is embedded within a molecular cloud, which is a good indicator of whether it is a cluster member.
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{images/surveycomp}\\
	\caption{A composite of the MIRES, MOXC and MPCM surveys in the region of NGC 2264.}
	\label{survcomp}
\end{figure}

\paragraph{}
Data from the MIRES survey is taken from multiple MIR and NIR sources to get as much of a spectrum as possible, NIR data was taken from the UKIDSS and 2MASS surveys \cite{feigelson2013overview}, while MIR data is taken from the \textit{Spitzer} space telescope. 2MASS is an all sky survey that has a shallower field than UKIDSS, and is used for brighter sources that cannot be found in the UKIDSS survey.

\paragraph{}
The final survey consisting of probable cluster members due to their IR excess emission and X-Ray emission are added to the MPCM (MYStIX Probable Complex Member catalogue, see figure \ref{survcomp}). In the case of NGC 2264, 1173 such sources are a part of the MPCM, table \ref{mpcm} shows an abundance of \textit{Chandra} sources, however, these are mostly extragalactic contaminants, \cite{townsley2014massive}, NGC 2264 also contains 524 likely YSO sources, all of which emit X-Rays, due to their proximity to molecular clouds.

\begin{table}[H]
\centering

\begin{tabular}{ccc}
\hline
\hline
\multicolumn{3}{c}{NGC 2264} \\
\textit{Chandra}/MOXC    & MIRES    & MPCM   \\ \hline
1328       & 805      & 1173   \\ \hline
\end{tabular}
\caption{Summary Source Populations for NGC 2264, source \cite{feigelson2013overview}}
\label{mpcm}
\end{table}

\subsection{X-Ray Data From MOXC}
\paragraph{}
The MSFRs Omnibus X-Ray Catalog (MOXC) is also used to find embedded sources, by finding X-Ray emission from shocks forming in the molecular cloud due to interaction from young hot stars \cite{townsley2014massive}. Point source data is provided by the \textit{Chandra} X-Ray telescope, and is convolved with the data from MIRES.
\paragraph{}
As X-Ray emission is in part produced by interactions between a star and the molecular cloud, X-Ray emission sources may be wrongly associated with field stars instead of cluster members, meaning that "proximity-only" matching IR and X-Ray data cannot be performed. In addition this problem is compounded by the poor angular resolution of X-Ray telescopes such as \textit{Chandra}; hence, Bayesian statistics are used to convolve MIRES and MOXC successfully \cite{naylor2013bayesian}. This does lead to potential problems within the sample, as the statistical matching may not be completely accurate, especially in denser regions of space, which may have foreground and background field stars.

\section{Clustering Algorithms}
\paragraph{}
After data is downloaded, the determination of subclusters within the structure can begin, this is achieved by the use of a clustering algorithm. Algorithms consists of two major categories, supervised and unsupervised methods.

\paragraph{}
The earliest, and simplest method of unsupervised subclustering is the "friends of friends" algorithm \cite{sneath1957application}, initially developed for taxonomy, was later found to be useful for determining cluster members. In simple unsupervised methods such as FoF, subclustering is entirely distance based and defined by density. Supervised methods add discrimination rules such as colour classification. A common example of this is the mixture model, with adjustable parameters.
\subsection{Previous Project}
\paragraph{}
\cite{GortonIR} uses a combination of DBSCAN, a density based clustering algorithm, developed by \cite{ester1996density}, in order to remove potential non-cluster objects; remaining stars are assumed to be cluster members. After this the average linkage clustering method is utilised, which constructs a dendrogram, combining clusters into higher level clusters in a hierarchy, such that the distance between 2 clusters $A$ and $B$ is taken to be the average distance between all member pairs $a,b$ such that the linkage criteria is:

\begin{equation}\label{link} \frac{1}{\left|A\right|\left|B\right|}\sum_{a\in A}\sum_{b\in B} d\left(a,b\right)
\end{equation}

\paragraph{}
To further reduce the number of field stars, colour cuts are used to isolate all but YSOs, which have a fairly flat, featureless red spectrum. However, Gorton has not taken into account edge cases, stars that are not deeply embedded in the star forming regions, and thus have a less reddened, more feature rich spectrum. These would be entirely removed before the algorithm runs. In addition, the subclustering model assumes a critical density for cluster members, which potentially biases the result. Finally, $F_8.0$\footnote{$8mm$ flux data} data could not be acquired for a majority of cluster members, meaning that rudimentary colour cuts not factoring $F_{8.0}$ data were used, despite the consensus that $F_{8.0}$ data is required for accurate colour cuts. Another method, such as utilising age data in conjunction with milder colour cuts, could potentially yield more accurate results.
\subsection{Subclustering by MYStIX}
\paragraph{}
The MYStIX survey uses a modified finite mixture model \cite{kuhn2014spatial}, which models spatial distributions of subclusters using isothermal ellipsoids. Ellipsoids are used as they are a reasonable shape for a subcluster formed from a collapsing molecular cloud. In addition, multiple models are used and selected based on aptness for the particular cluster using the Akaike Information Criterion (AIC). This is a significantly more involved and accurate method than \cite{GortonIR} and \cite{BoyerIR} utilised, however is beyond the scope of what is attainable in terms of complexity for this project.

\paragraph{}
The finite mixture model is powerful in that subclusters can be nested, which is exceptionally useful, as subclusters may be in front of or behind others, something that is not covered with less invovled techniques, secondly, a critical density, such as the DBSCAN derived method in \cite{GortonIR}, is not assumed, a successful flexible model should have as few assumptions as possible to be truly effective. As previously stated however, the method is extremely involved and computationally intense, finally, the model form (isothermal ellipsoid) must be assumed, While most subclusters are ellipsoidal in shape, and is a good assumption to make, it may not cover all possible subclusters; in particular, evolved, dispersed subclusters and ones with filamentary shapes may not be properly catalogued, or their borders may be incorrectly defined.
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{images/finite}
	\caption{Finite mixture subclustering example from the Orion star-forming region, source \cite{kuhn2014spatial}}
	\label{finite}
\end{figure}

\section{Potential Improvements For Next Years Project - The Gaia Survey}\label{tgas}
\paragraph{}
Usage of the Tycho-Gaia Astrometric Survey (TGAS) \cite{michalik2015tycho} was proposed at the beginning of this project in order to map clusters accurately in 3-D, however this survey, which bootstraps parallaxes using a combination of Gaia and Hipparcos parallax data is not accurate beyond distances of 1Kpc, as can be seen in figures \ref{tgas1} \& \ref{tgas2}, to see if this was not due to the sources themselves, a large section of data was downloaded from TGAS, with the distance and errors calculated using TOPCAT (figure \ref{errorprop}). There is a strong correlation between distance and propagation, regardless of the position of sources.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{images/hyades-1}
	\caption{A 3-D position plot of the Hyades cluster}
	\label{tgas1}
\end{figure}

\paragraph{}
In addition, at distances out to 1Kpc, a large number of cluster members are not even catalogued within TGAS, which means that accurate subclustering is not possible. This lack of catalogue members is due to the nature of the survey, which requires data from the Hipparcos survey which has a much shallower field, and will not have data on dim stars or embedded stars with a high degree of extinction at the required distances.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{images/blanco-1}
	\caption{A 3-D position plot of the cluster Blanco-1}
	\label{tgas2}
\end{figure}

\paragraph{}
The full Gaia parallax data should be available late 2017, and should provide parallaxes accurate to within 20\% out to $\approx 22$Kpc \cite{lindegren2007gaia}.



\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{images/Error}
	\caption{Uncertainty propagation due to increasing distance of sources}
	\label{errorprop}
\end{figure}

\section{Discussion \& Conclusion}
\paragraph{}
\cite{GortonIR} and \cite{BoyerIR} have produced algorithms that produce adequate results for subclustering structure, however steps can be made to improve upon the accuracy, especially by considering alternatives to the rough colour cut method used by both papers to determine cluster members. By potentially biasing the cluster member selection and subclustering method using stellar age data, subclustering may be more precise, with a greater number of potential members considered.

\paragraph{}
HR and lithium ageing techniques will primarily be used, with other metrics being discarded due to model dependence or unavailable data, most lithium data is available in the form of relative ages, which may still be useful, but attempts will be made to bootstrap this data with absolute ages if required. While it is a foregone conclusion that this process may not be as accurate as the MYStIX subclustering algorithm, it may hold up better than previous attempts.


\bibliography{bib}
\bibliographystyle{plain}
\end{multicols}


\end{document}
