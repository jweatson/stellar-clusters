# -*- coding: utf-8 -*-
"""
Created on Mon Mar 27 17:52:57 2017

@author: joseph
"""

"""
This is a plotting program for fancying up my graphs for my report
Input file should consist of a 3 or 6 column .csv file with headers
1. x axis data
2. x axis error bars
3. y axis data
4. y axis error bars
5. z axis data
6. z axis error bars

If error bars are not required, turn the flag errorbars to "False"
Outputs can be either:
- PDF output
- MP4 video output of rotating 3D plots
"""

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("Agg")
import matplotlib.animation as manimation
from mpl_toolkits.mplot3d import axes3d
import matplotlib.patheffects as pe
import numpy as np
import time
t1=time.time()
t=time.time()
#DATA IMPORTING
importname="WFCAM-col-abundance/corrected.csv"
plotdata=np.genfromtxt(importname,delimiter=",",skip_header=1)
print "Data imported... Took %.4f seconds" % (time.time()-t)
t=time.time()

def vidoutput( fig,writer,videofilename,ax,dpi ):
    t=time.time()
    with writer.saving(fig, videofilename,
                       dpi):
        for angle in np.arange(0,360,0.5):
                ax.view_init(30, angle)
                plt.draw()
                writer.grab_frame()
    print "Video output finished... Took %.4f seconds" % (time.time()-t)


def plotter( plotdata ):
    t=time.time()
    #SETTINGS
    values=2
    plottype="3D" #Toggle between 2D, 3D and comp plots, 3D plots can render out into video
    angle3d=45
    output_name="col-ha-corr"
    title="Colour-Halpha Plot"
    xlabel="J-K"
    ylabel="H-K"
    zlabel="Halpha Line Width (Ang)"
    xcol=2
    ycol=3
    zcol=6
    zerrcol=7
    errorbars=True
    zerrorbars=True #Display only z error bars?
    shadows=False
    videooutput=False #Used to render the output of a 3D plot as a rotating plot in .mp4 format
    imageoutput=True
    framerate=60 #Output framerate in fps, 60fps is buttery, 30fps might be more compatible with .ppt files
    bitrate=4500 #Video output bitrate in kilobits
    alpha=1.0
    t=time.time()




    #filenames, metadata, initial setup
    metadata = dict(title=output_name,
                    artist="Joseph Eatson",
                    comment="CYGNUS-OB2")
    FFMpegWriter = manimation.writers["ffmpeg"]
    writer = FFMpegWriter(fps=framerate,
                          bitrate=bitrate,
                          extra_args=["-pix_fmt", "yuv420p","-preset","veryslow"],
                          metadata=metadata,
                          )
    imagefilename="Graphs/Spruced/" + output_name + ".pdf"
    videofilename="Graphs/Video/" + output_name +".mp4"
    if plottype=="3D":
        dpivid=270
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)

        X=plotdata[0:,xcol]
        Y=plotdata[0:,ycol]
        Z=plotdata[0:,zcol]

        if errorbars==True:
            zerror=plotdata[0:,zerrcol]
            for i in np.arange(0,len(X)):
                ax.plot([X[i],X[i]], [Y[i],Y[i]], zs=[Z[i]+zerror[i], Z[i]-zerror[i]],
                        marker="_",c="#1f77b4",alpha=alpha
                        )
            if zerrorbars==False:
                xerror=plotdata[0:,1]
                yerror=plotdata[0:,3]
                for i in np.arange(0,len(X)):
                    ax.plot([X[i]+xerror[i], X[i]-xerror[i]], [Y[i], Y[i]], zs=[Z[i], Z[i]],
                            marker="_",c="r",alpha=alpha
                            )
                    ax.plot([X[i], X[i]], [Y[i]+yerror[i], Y[i]-yerror[i]], zs=[Z[i], Z[i]],
                            marker="_",c="g",alpha=alpha
                            )

        if errorbars==False:
            ax.scatter(X, Y, Z,alpha=alpha)
        if videooutput == True:
            vidoutput( fig=fig,
                      writer=writer,
                      videofilename=videofilename,
                      ax=ax,
                      dpi=dpivid )
        if imageoutput == True:
            t=time.time()
            ax.view_init(30,angle3d)
            plt.savefig(imagefilename,
                        dpi=300,
                        bbox_inches="tight")
            print "Image output finished... Took %.4f seconds" % (time.time()-t)


    if plottype=="2D":
        plt.figure()
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        cols=["b","g","r","c","m","y","k"]
        for n in range(0,(values)):
            X=plotdata[0:,n]
            Y=plotdata[0:,(n+1)]
            plt.scatter(X,Y,c=cols[n])

    if plottype=="comp":
        X=plotdata[0:,xcol]
        Y=plotdata[0:,ycol]
        Z=plotdata[0:,zcol]
        fig = plt.figure(figsize=(10,7))
        ax = fig.add_subplot(221, projection="3d")
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        ax.set_zlabel(zlabel)
        ax.tick_params(axis='both', which='major', labelsize=8)
        ax.tick_params(axis='both', which='minor', labelsize=8)
        ax.view_init(30,angle3d)
        if shadows==True:
            ax.scatter(X, Y, c='k', marker='.', alpha=0.1)
        if errorbars==True:
            zerror=plotdata[0:,zerrcol]
            for i in np.arange(0,len(X)):
                ax.plot([X[i],X[i]], [Y[i],Y[i]], zs=[Z[i]+zerror[i], Z[i]-zerror[i]],
                        marker="_",c="#1f77b4",alpha=alpha
                        )
            if zerrorbars==False:
                xerror=plotdata[0:,1]
                yerror=plotdata[0:,3]
                for i in np.arange(0,len(X)):
                    ax.plot([X[i]+xerror[i], X[i]-xerror[i]], [Y[i], Y[i]], zs=[Z[i], Z[i]],
                            marker="_",c="r",alpha=alpha
                            )
                    ax.plot([X[i], X[i]], [Y[i]+yerror[i], Y[i]-yerror[i]], zs=[Z[i], Z[i]],
                            marker="_",c="g",alpha=alpha
                            )

        if errorbars==False:
            ax.scatter(X, Y, Z,alpha=alpha)

        fig.add_subplot(222)
        plt.scatter(X,Y,marker=".")
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        fig.add_subplot(223)
        if errorbars==True:
            if zerrorbars==True:
                plt.errorbar(X, Z, yerr=zerror,
                             fmt=" ",
                             capsize=2)
        if errorbars==False:
            plt.scatter(X,Z)
        plt.xlabel(xlabel)
        plt.ylabel(zlabel)
        fig.add_subplot(224)
        if errorbars==True:
            if zerrorbars==True:
                plt.errorbar(Y, Z, yerr=zerror,
                             fmt=" ",
                             capsize=2)
        if errorbars==False:
            plt.scatter(Y,Z)
        plt.xlabel(ylabel)
        plt.ylabel(zlabel)
        plt.suptitle(title,
                     fontsize=16)
        t=time.time()
        plt.savefig(imagefilename,
                    dpi=300,
                    bbox_inches="tight")
        print "Image output finished... Took %.4f seconds" % (time.time()-t)
        if videooutput == True:
            dpivid=160
            vidoutput( fig=fig,
                      writer=writer,
                      videofilename=videofilename,
                      ax=ax,
                      dpi=dpivid )
    return

print "Plotted... " + str(len(plotdata[0:,1])) + " objects!"

plotter( plotdata=plotdata )
print "Finished! Took %.4f seconds total" % (time.time()-t1)
