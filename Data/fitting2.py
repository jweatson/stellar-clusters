#Polynomial fitting, flipped axes
import numpy as np
import matplotlib.pyplot as plt
#Data importing
data=np.loadtxt("4setsfitting.csv",skiprows=1,delimiter=",")
#Polynomial fitting of various orders
fit1=np.polyfit(data[0:,2],data[0:,3],1)
fit2=np.polyfit(data[0:,2],data[0:,3],2)
fit3=np.polyfit(data[0:,2],data[0:,3],3)
fit4=np.polyfit(data[0:,2],data[0:,3],4)
#Generation of x data for fitting
x=np.arange(0,1,0.01)
#Fitting
fitting1=np.poly1d(fit1)
fitting2=np.poly1d(fit2)
fitting3=np.poly1d(fit3)
fitting4=np.poly1d(fit4)
y1=fitting1(x)
y2=fitting2(x)
y3=fitting3(x)
y4=fitting4(x)
#Plotting
plt.plot(x,y1,label="$1^{st}$ order")
plt.plot(x,y2,label="$2^{nd}$ order")
plt.plot(x,y3,label="$3^{rd}$ order")
plt.plot(x,y4,label="$4^{th}$ order")
plt.scatter(data[0:,2],data[0:,3],marker='s',label="Combined database")
plt.ylabel("Age (yr)")
plt.ylim(1e6,10e6)
plt.xlabel("Lithium Equivalent Line Width ($ \AA $)")
plt.title("Polynomial fitting for bootstrapping")
plt.legend(prop={'size':8})
plt.savefig("Graphs/Fitting2",dpi=300)