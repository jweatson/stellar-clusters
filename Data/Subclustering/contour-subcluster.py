# -*- coding: utf-8 -*-
"""
Created on Sun Apr 16 13:30:38 2017

@author: jweat
"""

from astropy.wcs import WCS
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.mlab as mlab

def pixcorrection(inp):
    xcorr=1109.81
    xcons=100.988
    ycorr=-1125.05
    ycons=8.52076
    ra=xcorr(xcons-inp[0:,0])
    dec=ycorr(ycons-inp[0:,1])
    return zip(ra,dec)

mpcm=np.genfromtxt("../Herschel/Hcatalog-DEG.csv",
                           delimiter=",",)
contour=np.genfromtxt("contours.csv",
                      delimiter=",")
plotcorr=pixcorrection(inp=mpcm)
contcorr=pixcorrection(inp=contour)



filename="../../../Herschel/pacs70-cal.fits"
herschel_image = fits.getdata(filename, ext=0)
herschel_coordinates = WCS(filename)
hdu = fits.open(filename)[0]
wcs = WCS(hdu.header)



contour_z=contour[0:,2:len(contour)]
contplot=zip()



contour_x=1109.81*(100.988 - contour[0:,0])
contour_y=-1125.05*(8.52076 - contour[0:,1])
contour_z=contour[0:,2:502]