#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 16:41:19 2017

@author: py13je
"""
import matplotlib.pyplot as plt
import random
import csv
x=[]
y=[]
for _ in range(3):
    mu=random.random()*5
    sig=random.random()*5
    for _ in range(100):
        x.append((random.gauss(mu,sig)*1))
        y.append((random.gauss(mu,sig)*1))
xy=zip(x,y)
plt.scatter(x,y)

def savedata():
    plt.scatter(x,y)
    plt.savefig("clustertest1.png",dpi=600)
    with open("clustertest1.csv","wb") as f:
        writer=csv.writer(f)
        writer.writerow(str("RA\,DEC"))
        writer.writerows(xy)