import numpy as np
import matplotlib.pyplot as plt
printcounter,totalcounter=0,0
inputs=["Mystix/MPCM.csv",
"Herschel/Hcatalog-Finalmembers-Rreadable",
"Seed Transfer/Protostars/protostars_readable.csv"]
outputs=["mystix-dmat.pdf",
"herschel-dmat.pdf",
"protostars-dmat.pdf"]
for k in range(len(inputs)):
    data=np.genfromtxt(inputs[k],delimiter=",")
    distances=[]
    for i in range(0,len(data)):
        cache2=[]
        for j in range(0,len(data)):
            cache2.append(np.hypot(data[i][0]-data[j][0],data[i][1]-data[j][1]))
            cache2.sort()
            printcounter+=1
            totalcounter+=1
            if printcounter==1000:
                print"Finished "+str(totalcounter)+" iterations"
                printcounter=0
        distances.append(cache2[2])
    n=range(len(distances))
    distances.sort()    
    fig = plt.figure()
    plt.plot(distances)
    plt.title("Sorted Distance Matrix")
    plt.ylabel("Distance (deg)")
    plt.savefig(outputs[k],
                dpi=300,
                bbox_inches="tight")
    plt.show()
    print "Finished " + outputs[k]