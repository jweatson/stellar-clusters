from astropy.wcs import WCS
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy import stats as st
#MPCM_southern_region_readable.csv
title="MPCM/Protostars Seeded Subclustering"
mode="members" #mode can be members or centres
mpcm_centres=np.genfromtxt("../../Outputs/protostars-mpcm-centres.csv",
                           delimiter=",")
precursor_centres=np.genfromtxt("../../Outputs/protostars-precursor-centres.csv",
                               delimiter=",")
precursors=np.genfromtxt("protostars_southern_region_readable.csv",
                         delimiter=",")
members=np.genfromtxt("MPCM_southern_region_readable.csv",
                      delimiter=",")
members_sub=np.genfromtxt("../../Outputs/protostars-DBSCAN.csv",
                          delimiter=",")
contour_name="protostars-kmeans-"
plotname="../../../Graphs/Spruced/PROTOSTAR-SUBCLUSTER-COMPARISON.pdf"
n_contours=3
filename="../../../Herschel/pacs70-cal.fits"
herschel_image = fits.getdata(filename, ext=0)
herschel_coordinates = WCS(filename)
hdu = fits.open(filename)[0]
wcs = WCS(hdu.header)
fig = plt.figure(figsize=(8,10))
ax=fig.add_subplot(111, projection=wcs)
trans=ax.get_transform("world")
ax.imshow(herschel_image,
           cmap="gray",
           norm=LogNorm(vmin=0.01,
                        vmax=1))

plt.xlim(650,
         1050)
plt.ylim(950,
         1550)

#plt.xlim(900,
#         700)
#plt.ylim(950,
#         1250)
if mode == "members":
#    ax.scatter(members[0:,0],members[0:,1],
#               transform=trans,
#               alpha=0.6,
#               color="0.75",
#               linewidth="0.5",
#               edgecolor="w",
#               label="MPCM Members")
    ax.scatter(members_sub[0:,1],
               members_sub[0:,2],
               c=members_sub[0:,3],
               linewidth="0.5",
               marker="s",
               edgecolor="w",
               transform=trans,
               label="Subcluster groups")
    
    cols=["w","y","c","m","g","r"]
    for n in range(1,n_contours+1):
        fname="../../Outputs/"+contour_name+str(n)+".csv"
        clustlab="Cluster "+str(n)+" Contour"
        contdata=np.genfromtxt(fname,delimiter=",")
        contx=contdata[1:,1]
        conty=contdata[1:,2]
        xmin=min(members[1:,0])
        xmax=max(members[1:,0])
        ymin=min(members[1:,1])
        ymax=max(members[1:,1])  
        xx,yy=np.mgrid[xmin:xmax:1000j,
                       ymin:ymax:1000j]
        positions=np.vstack([xx.ravel(),
                             yy.ravel()])
        values=np.vstack([contx.ravel(),
                          conty.ravel()])
        kernel = st.gaussian_kde(values)
        f = np.reshape(kernel(positions).T,
                       xx.shape)
        cset=ax.contour(xx,yy,f,colors=cols[n-1],
                        transform=trans,
                        label=clustlab,
                        levels=[3*np.std(f),4*np.std(f),5*np.std(f)])
        ticks=range(0,200,10)
        print np.std(f)
        #ax.clabel(cset, inline=1,
         #         fontsize=8,ticks=ticks,
          #        fmt = '%1.0f',)

if mode == "centres":
    for n in range(1,n_contours+1):
        ax.arrow(precursor_centres[n,1],
                 precursor_centres[n,2],
                 mpcm_centres[n,1]-precursor_centres[n,1],
                 mpcm_centres[n,2]-precursor_centres[n,2],
                 head_width=0, head_length=0,
                 color="r",
                 transform=trans,
                 length_includes_head=True)
    ax.scatter(precursor_centres[1:,1],
               precursor_centres[1:,2],
               s=10**2,
               marker="v",
               c="b",
               label="Precursor Centroids",
               transform=trans)
    ax.scatter(mpcm_centres[1:,1],
               mpcm_centres[1:,2],
               s=10**2,
               marker="^",
               c="r",
               label="MPCM Centroids",
               transform=trans)
    ax.scatter(precursors[0:,0],
               precursors[0:,1],
               transform=trans,
               color="r",
               linewidth="0.5",
               edgecolor="w",
               label="Precursor Dataset Members")
    

plt.title(title)
plt.xlabel("RA (deg)")
plt.ylabel("DEC (deg)")
#plt.legend(prop={'size':8})
plt.savefig(plotname,
            dpi=600,
            bbox_inches="tight")
plt.show()