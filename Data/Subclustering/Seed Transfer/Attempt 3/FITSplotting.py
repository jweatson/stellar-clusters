from astropy.wcs import WCS
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

mpcm_centres=np.genfromtxt("mpcm_centres.csv",
                           delimiter=",",)
mpcm_x=1109.81*(100.988 - mpcm_centres[0:,0])
mpcm_y=-1125.05*(8.52076 - mpcm_centres[0:,1])

herschel_centres=np.genfromtxt("herschel_centres.csv",
                               delimiter=",",)
herschel_x=1109.81*(100.988 - herschel_centres[0:,0])
herschel_y=-1125.05*(8.52076 - herschel_centres[0:,1])

filename="../../../Herschel/pacs70-cal.fits"
herschel_image = fits.getdata(filename, ext=0)
herschel_coordinates = WCS(filename)
hdu = fits.open(filename)[0]
wcs = WCS(hdu.header)

size=8**2
alph=0.8
plt.imshow(herschel_image,
           cmap="hot",
           norm=LogNorm(vmin=0.01,
                        vmax=1),
                        )
plt.colorbar(label="70 Micron Flux (Jy)")
plt.scatter(herschel_x,herschel_y,
            s=size,
            c="b",
            alpha=alph,
            label="Herschel Centroids")
plt.scatter(mpcm_x,mpcm_y,
            s=size,
            c="r",
            alpha=alph,
            label="4 Stage Centroids")
plt.xlim(1250,
         500)
plt.ylim(850,
         1650)
for n in range(0,6):
    plt.arrow(herschel_x[n],herschel_y[n],mpcm_x[n]-herschel_x[n],mpcm_y[n]-herschel_y[n],
              head_width=10, head_length=10,
              #fc='b', ec='b',
              alpha=alph,
              length_includes_head=True)
plt.title("Centroid Drift Between 2nd and 4th Stage")
plt.xlabel("X (Pixels)")
plt.ylabel("Y (Pixels)")
plt.legend(prop={'size':8})
plt.show()