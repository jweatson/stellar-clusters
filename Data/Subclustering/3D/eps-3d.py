# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 23:57:44 2017

@author: jweat
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import distance
printcounter,totalcounter=0,0
inputs=["3dout.csv"]
outputs=["3d-eps.pdf"]
for k in range(len(inputs)):
    data=np.genfromtxt(inputs[k],delimiter=",")
    distances=[]
    for i in range(0,len(data)):
        cache2=[]
        for j in range(1,len(data)):
            cache2.append(distance.euclidean(data[i],data[j]))
            cache2.sort()
            printcounter+=1
            totalcounter+=1
            if printcounter==1000:
                print"Finished "+str(totalcounter)+" iterations"
                printcounter=0
        distances.append(cache2[1])
    n=range(len(distances))
    distances.sort()    
    fig = plt.figure()
    plt.plot(distances)
    plt.title("Sorted Distances")
    plt.ylabel("Distance (pc)")
    plt.savefig(outputs[k],
                dpi=300,
                bbox_inches="tight")
    plt.show()
    print "Finished " + outputs[k]