#Exponential fitting
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
#Exponential function definition
def exp(x, a, b, c):
    return a * np.exp(-b * x) + c
#Data importing
data=np.loadtxt("4setsfitting.csv",skiprows=1,delimiter=",")
#Fitting and optimisation
guess=[1e6,1,1]
x=np.arange(0,1,0.01)
par,mat=curve_fit(exp,data[0:,2],data[0:,3],p0=guess,maxfev=10000)
fit=exp(x,*par)
#Plotting
plt.plot(x,fit,label="Exponential Fit")
plt.scatter(data[0:,2],data[0:,3],marker='s',label="Combined database")
plt.ylabel("Age (yr)")
plt.ylim(1e6,10e6)
plt.legend(prop={'size':8})
plt.xlabel("Lithium Equivalent Line Width ($ \AA $)")
plt.ylim(0,1e7)
plt.xlim(0,1)
plt.title("Exponential fitting for bootstrapping")
plt.savefig("Graphs/expfitting",dpi=300)