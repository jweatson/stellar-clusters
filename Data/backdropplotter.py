# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 16:01:10 2017

@author: joseph
"""

from astropy.wcs import WCS
import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.mlab as mlab


mpcm_centres=np.genfromtxt("Subclustering/Seed Transfer/Attempt 3/mpcm_centres.csv",
                           delimiter=",",)
bouvier=np.genfromtxt("CE Datasets/bouvier_aged.csv",
                      delimiter=",")
dahm=np.genfromtxt("CE Datasets/dahm_aged.csv",
                   delimiter=",")
sergison=np.genfromtxt("CE Datasets/sergison_aged.csv",
                       delimiter=",")
lim=np.genfromtxt("CE Datasets/lim_aged.csv",
                  delimiter=",")


mpcm_x=1109.81*(100.988 - mpcm_centres[0:,0])
mpcm_y=-1125.05*(8.52076 - mpcm_centres[0:,1])
bouvier_x=1109.81*(100.988 - bouvier[0:,0])
bouvier_y=-1125.05*(8.52076 - bouvier[0:,1])
dahm_x=1109.81*(100.988 - dahm[0:,0])
dahm_y=-1125.05*(8.52076 - dahm[0:,1])
sergison_x=1109.81*(100.988 - sergison[0:,0])
sergison_y=-1125.05*(8.52076 - sergison[0:,1])
lim_x=1109.81*(100.988 - lim[0:,0])
lim_y=-1125.05*(8.52076 - lim[0:,1])

filename="Herschel/pacs70-cal.fits"
herschel_image = fits.getdata(filename, ext=0)
herschel_coordinates = WCS(filename)
hdu = fits.open(filename)[0]
wcs = WCS(hdu.header)

size=3**2
alph=0.9
fig=plt.figure()
fig.add_subplot(111,projection=wcs)
plt.imshow(herschel_image,
           cmap=plt.cm.viridis,
           norm=LogNorm(vmin=0.01,
                        vmax=1),
                        )
#plt.scatter(mpcm_x,mpcm_y,
#            s=size,
#            c="r",
#            alpha=alph,
#            label="3 Stage Centroids")
plt.scatter(dahm_x,dahm_y,
            s=size,
            c="g",
            alpha=alph,
            label="Dahm",
            marker="^")
plt.scatter(lim_x,lim_y,
            s=size,
            c="y",
            alpha=alph,
            label="Lim",
            marker="D")
plt.scatter(bouvier_x,bouvier_y,
            s=size,
            c="r",
            alpha=alph,
            label="Bouvier",
            marker="s")
plt.scatter(sergison_x,sergison_y,
            s=size,
            c="w",
            alpha=alph,
            label="Sergison")


plt.xlim(1250,
         500)
plt.ylim(850,
         1650)
    
plt.title("Matched Catalogues")
plt.xlabel("RA (deg)")
plt.ylabel("DEC (deg)")
plt.legend(prop={'size':8})
plt.savefig("Graphs/Spruced/allsources-aged.png",
            dpi=600,
            bbox_inches="tight"
            )