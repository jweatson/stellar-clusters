#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 16:34:21 2017

@author: joseph
"""
from isochrones import StarModel
from isochrones.mist import MIST_Isochrone

Teff=4326,114
J=12.160,0.12
K=11.692,0.11

mist = MIST_Isochrone()
model  = StarModel(mist, Teff=Teff, J=J, K=K)
model.fit()
