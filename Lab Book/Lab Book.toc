\contentsline {section}{\numberline {1}Schedule}{2}{section.1}
\contentsline {section}{\numberline {2}Weeks 1-3 - Initial Research}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}TGAS Astrometric data}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Lithium Abundance, and other spectroscopic features}{4}{subsubsection.2.1.1}
\contentsline {subsection}{\numberline {2.2}Target - NGC 2264}{4}{subsection.2.2}
\contentsline {paragraph}{}{5}{section*.8}
\contentsline {section}{\numberline {3}Previous Projects by University Students}{5}{section.3}
\contentsline {paragraph}{}{5}{section*.9}
\contentsline {paragraph}{}{5}{section*.10}
\contentsline {section}{\numberline {4}Ageing and Bootstrapping}{5}{section.4}
\contentsline {paragraph}{}{5}{section*.11}
\contentsline {subsection}{\numberline {4.1}Matching of lithium and aged stars}{6}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Lim Lithium Survey}{6}{subsubsection.4.1.1}
\contentsline {paragraph}{}{6}{section*.13}
\contentsline {paragraph}{}{6}{section*.14}
\contentsline {paragraph}{}{7}{section*.15}
\contentsline {subsubsection}{\numberline {4.1.2}Bouvier Lithium Survey}{8}{subsubsection.4.1.2}
\contentsline {paragraph}{}{8}{section*.18}
\contentsline {paragraph}{}{8}{section*.19}
\contentsline {paragraph}{}{8}{section*.20}
\contentsline {subsubsection}{\numberline {4.1.3}Sergison Lithium Survey}{8}{subsubsection.4.1.3}
\contentsline {paragraph}{}{8}{section*.22}
\contentsline {paragraph}{}{8}{section*.23}
\contentsline {subsection}{\numberline {4.2}H$^\alpha $ Surveys}{9}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Dahm H$^\alpha $ and Lithium Survey}{9}{subsubsection.4.2.1}
\contentsline {paragraph}{}{9}{section*.25}
\contentsline {paragraph}{}{9}{section*.26}
\contentsline {subsection}{\numberline {4.3}Bootstrapping}{11}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Polynomial Fitting}{11}{subsubsection.4.3.1}
\contentsline {paragraph}{}{13}{section*.32}
\contentsline {paragraph}{}{13}{section*.33}
\contentsline {subsubsection}{\numberline {4.3.2}Exponential Fitting}{13}{subsubsection.4.3.2}
\contentsline {paragraph}{}{13}{section*.34}
\contentsline {paragraph}{}{15}{section*.37}
\contentsline {subsubsection}{\numberline {4.3.3}Conclusion}{15}{subsubsection.4.3.3}
\contentsline {paragraph}{}{15}{section*.38}
\contentsline {paragraph}{}{15}{section*.39}
\contentsline {section}{\numberline {5}Finding Other Useful Correlations}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Isochrone fitting}{15}{subsection.5.1}
\contentsline {paragraph}{Method}{15}{section*.41}
\contentsline {paragraph}{Conclusion}{15}{section*.42}
\contentsline {paragraph}{}{15}{section*.43}
\contentsline {subsection}{\numberline {5.2}Finding Correlations Using R}{16}{subsection.5.2}
\contentsline {paragraph}{}{16}{section*.46}
\contentsline {paragraph}{}{17}{section*.47}
\contentsline {paragraph}{}{17}{section*.48}
\contentsline {paragraph}{}{17}{section*.51}
\contentsline {paragraph}{}{18}{section*.52}
\contentsline {subsubsection}{\numberline {5.2.1}An Aside: FU-Orionis Candidates}{18}{subsubsection.5.2.1}
\contentsline {paragraph}{}{18}{section*.55}
\contentsline {subsection}{\numberline {5.3}Subclustering Through Clumps}{19}{subsection.5.3}
\contentsline {paragraph}{}{19}{section*.57}
\contentsline {paragraph}{}{19}{section*.58}
\contentsline {paragraph}{}{20}{section*.61}
\contentsline {section}{\numberline {6}Collection of a Complete Independent Database of NGC 2264}{20}{section.6}
\contentsline {paragraph}{}{20}{section*.63}
\contentsline {paragraph}{}{20}{section*.64}
\contentsline {paragraph}{}{20}{section*.65}
\contentsline {paragraph}{}{21}{section*.66}
\contentsline {subsection}{\numberline {6.1}Addition of Herschel Data}{21}{subsection.6.1}
\contentsline {paragraph}{}{21}{section*.67}
\contentsline {paragraph}{}{21}{section*.68}
\contentsline {paragraph}{}{22}{section*.72}
\contentsline {section}{\numberline {7}Subclustering}{22}{section.7}
\contentsline {subsection}{\numberline {7.1}Testing Subclustering Algorithms}{22}{subsection.7.1}
\contentsline {paragraph}{}{22}{section*.73}
\contentsline {paragraph}{}{22}{section*.74}
\contentsline {paragraph}{}{23}{section*.76}
\contentsline {paragraph}{}{27}{section*.82}
\contentsline {paragraph}{}{27}{section*.83}
\contentsline {paragraph}{}{28}{section*.86}
\contentsline {subsection}{\numberline {7.2}Dr. Buckners Catalogue}{30}{subsection.7.2}
\contentsline {paragraph}{}{30}{section*.89}
\contentsline {paragraph}{}{31}{section*.91}
\contentsline {paragraph}{}{34}{section*.96}
\contentsline {subsubsection}{\numberline {7.2.1}4-Stage Subclustering Script}{34}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Subclustering Plotting Script}{35}{subsubsection.7.2.2}
\contentsline {paragraph}{}{37}{section*.97}
\contentsline {subsection}{\numberline {7.3}3-D Subclustering}{38}{subsection.7.3}
\contentsline {paragraph}{}{38}{section*.99}
\contentsline {paragraph}{}{39}{section*.101}
\contentsline {paragraph}{}{39}{section*.102}
\contentsline {paragraph}{}{39}{section*.103}
\contentsline {paragraph}{}{40}{section*.105}
\contentsline {paragraph}{}{40}{section*.107}
