\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {paragraph}{}{1}{section*.3}
\contentsline {paragraph}{}{1}{section*.4}
\contentsline {paragraph}{}{1}{section*.5}
\contentsline {paragraph}{}{1}{section*.6}
\contentsline {paragraph}{}{1}{section*.7}
\contentsline {paragraph}{}{1}{section*.8}
\contentsline {subsection}{\numberline {1.1}Subclustering}{1}{subsection.1.1}
\contentsline {paragraph}{}{1}{section*.9}
\contentsline {paragraph}{}{2}{section*.11}
\contentsline {paragraph}{}{2}{section*.13}
\contentsline {paragraph}{}{2}{section*.14}
\contentsline {paragraph}{}{2}{section*.15}
\contentsline {paragraph}{}{2}{section*.16}
\contentsline {paragraph}{}{2}{section*.17}
\contentsline {section}{\numberline {2}Subclustering Algorithms}{2}{section.2}
\contentsline {paragraph}{}{2}{section*.18}
\contentsline {paragraph}{}{2}{section*.19}
\contentsline {subsection}{\numberline {2.1}Previous Project, DBSCAN and K-Means}{2}{subsection.2.1}
\contentsline {paragraph}{}{2}{section*.20}
\contentsline {paragraph}{}{2}{section*.21}
\contentsline {paragraph}{}{3}{section*.22}
\contentsline {paragraph}{}{3}{section*.23}
\contentsline {paragraph}{}{3}{section*.25}
\contentsline {paragraph}{}{3}{section*.26}
\contentsline {subsection}{\numberline {2.2}Multi-Stage Methods}{3}{subsection.2.2}
\contentsline {paragraph}{}{3}{section*.27}
\contentsline {subsection}{\numberline {2.3}Subclustering by MYStIX}{3}{subsection.2.3}
\contentsline {paragraph}{}{3}{section*.28}
\contentsline {paragraph}{}{3}{section*.29}
\contentsline {section}{\numberline {3}Potential Subclustering Methods}{4}{section.3}
\contentsline {paragraph}{}{4}{section*.31}
\contentsline {paragraph}{}{4}{section*.32}
\contentsline {paragraph}{}{4}{section*.33}
\contentsline {paragraph}{}{4}{section*.34}
\contentsline {subsection}{\numberline {3.1}Spatial Mapping}{4}{subsection.3.1}
\contentsline {paragraph}{}{4}{section*.35}
\contentsline {subsubsection}{\numberline {3.1.1}Introduction}{4}{subsubsection.3.1.1}
\contentsline {paragraph}{}{4}{section*.36}
\contentsline {paragraph}{}{4}{section*.38}
\contentsline {paragraph}{}{4}{section*.39}
\contentsline {subsubsection}{\numberline {3.1.2}Method}{5}{subsubsection.3.1.2}
\contentsline {paragraph}{}{5}{section*.42}
\contentsline {paragraph}{}{5}{section*.43}
\contentsline {paragraph}{}{5}{section*.44}
\contentsline {subsubsection}{\numberline {3.1.3}Performance and Assessment}{5}{subsubsection.3.1.3}
\contentsline {paragraph}{}{5}{section*.45}
\contentsline {paragraph}{}{5}{section*.47}
\contentsline {paragraph}{}{5}{section*.49}
\contentsline {paragraph}{}{6}{section*.51}
\contentsline {paragraph}{}{6}{section*.53}
\contentsline {paragraph}{}{6}{section*.54}
\contentsline {subsection}{\numberline {3.2}Ageing}{6}{subsection.3.2}
\contentsline {paragraph}{}{6}{section*.55}
\contentsline {subsubsection}{\numberline {3.2.1}Primary Cluster Catalogues - The MPCM \& MIRES Surveys}{6}{subsubsection.3.2.1}
\contentsline {paragraph}{}{6}{section*.56}
\contentsline {paragraph}{}{6}{section*.57}
\contentsline {paragraph}{}{6}{section*.58}
\contentsline {subsubsection}{\numberline {3.2.2}Surveys of the Cluster NGC 2264}{7}{subsubsection.3.2.2}
\contentsline {paragraph}{}{7}{section*.61}
\contentsline {paragraph}{}{7}{section*.62}
\contentsline {paragraph}{}{7}{section*.63}
\contentsline {paragraph}{}{7}{section*.64}
\contentsline {paragraph}{}{7}{section*.65}
\contentsline {paragraph}{}{7}{section*.66}
\contentsline {paragraph}{}{7}{section*.67}
\contentsline {subsubsection}{\numberline {3.2.3}Lithium}{7}{subsubsection.3.2.3}
\contentsline {paragraph}{}{7}{section*.69}
\contentsline {paragraph}{}{8}{section*.71}
\contentsline {subsubsection}{\numberline {3.2.4}H$^\alpha $ Line Width}{8}{subsubsection.3.2.4}
\contentsline {paragraph}{}{8}{section*.73}
\contentsline {paragraph}{}{8}{section*.74}
\contentsline {paragraph}{}{8}{section*.76}
\contentsline {subsubsection}{\numberline {3.2.5}Alternative Ageing Methods}{9}{subsubsection.3.2.5}
\contentsline {paragraph}{}{9}{section*.78}
\contentsline {paragraph}{}{9}{section*.80}
\contentsline {paragraph}{}{9}{section*.81}
\contentsline {paragraph}{}{9}{section*.82}
\contentsline {paragraph}{}{9}{section*.83}
\contentsline {paragraph}{}{10}{section*.86}
\contentsline {paragraph}{}{10}{section*.87}
\contentsline {paragraph}{Outliers}{10}{section*.89}
\contentsline {paragraph}{}{10}{section*.90}
\contentsline {paragraph}{}{10}{section*.91}
\contentsline {paragraph}{}{10}{section*.92}
\contentsline {paragraph}{}{10}{section*.94}
\contentsline {subsection}{\numberline {3.3}Extending Previous Project Subclustering Algorithms}{10}{subsection.3.3}
\contentsline {paragraph}{}{10}{section*.95}
\contentsline {paragraph}{}{10}{section*.96}
\contentsline {paragraph}{}{10}{section*.97}
\contentsline {paragraph}{}{10}{section*.98}
\contentsline {section}{\numberline {4}Targets}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}NGC 2264}{12}{subsection.4.1}
\contentsline {paragraph}{}{12}{section*.99}
\contentsline {paragraph}{}{12}{section*.100}
\contentsline {subsection}{\numberline {4.2}Hyades}{12}{subsection.4.2}
\contentsline {paragraph}{}{12}{section*.102}
\contentsline {paragraph}{}{12}{section*.104}
\contentsline {section}{\numberline {5}Multi-Stage Seeding}{12}{section.5}
\contentsline {paragraph}{}{12}{section*.105}
\contentsline {paragraph}{}{12}{section*.106}
\contentsline {paragraph}{}{13}{section*.107}
\contentsline {subsection}{\numberline {5.1}4-stage Subclustering in R}{13}{subsection.5.1}
\contentsline {paragraph}{}{13}{section*.109}
\contentsline {paragraph}{}{13}{section*.110}
\contentsline {paragraph}{}{13}{section*.113}
\contentsline {paragraph}{}{13}{section*.114}
\contentsline {paragraph}{}{13}{section*.115}
\contentsline {paragraph}{}{13}{section*.116}
\contentsline {paragraph}{}{13}{section*.117}
\contentsline {paragraph}{}{13}{section*.118}
\contentsline {paragraph}{}{14}{section*.119}
\contentsline {subsection}{\numberline {5.2}Seeding Using Embedded Protostars}{14}{subsection.5.2}
\contentsline {paragraph}{}{14}{section*.122}
\contentsline {paragraph}{}{14}{section*.125}
\contentsline {paragraph}{}{16}{section*.127}
\contentsline {paragraph}{}{16}{section*.130}
\contentsline {section}{\numberline {6}3-D Subclustering}{16}{section.6}
\contentsline {paragraph}{}{16}{section*.131}
\contentsline {paragraph}{}{16}{section*.132}
\contentsline {paragraph}{}{18}{section*.135}
\contentsline {paragraph}{}{18}{section*.139}
\contentsline {paragraph}{}{18}{section*.141}
\contentsline {paragraph}{}{18}{section*.142}
\contentsline {subsection}{\numberline {6.1}Kinematic Subclustering and the Moving-Cluster Method}{18}{subsection.6.1}
\contentsline {paragraph}{}{18}{section*.143}
\contentsline {paragraph}{}{19}{section*.144}
\contentsline {section}{\numberline {7}Discussion \& Comparison with Literature}{19}{section.7}
\contentsline {subsection}{\numberline {7.1}4-Stage Subclustering}{19}{subsection.7.1}
\contentsline {paragraph}{}{19}{section*.146}
\contentsline {paragraph}{}{19}{section*.147}
\contentsline {paragraph}{}{19}{section*.148}
\contentsline {paragraph}{}{20}{section*.150}
\contentsline {paragraph}{}{20}{section*.151}
\contentsline {subsection}{\numberline {7.2}Spatial Subclustering}{20}{subsection.7.2}
\contentsline {paragraph}{}{20}{section*.152}
\contentsline {paragraph}{}{21}{section*.155}
\contentsline {paragraph}{}{21}{section*.156}
\contentsline {paragraph}{}{21}{section*.157}
\contentsline {section}{\numberline {8}Conclusion}{21}{section.8}
\contentsline {paragraph}{}{21}{section*.158}
\contentsline {paragraph}{}{21}{section*.159}
\contentsline {paragraph}{}{21}{section*.160}
\contentsline {paragraph}{}{21}{section*.161}
\contentsline {paragraph}{}{22}{section*.162}
\contentsline {paragraph}{}{22}{section*.163}
\contentsline {section}{\numberline {9}Appendix}{i}{section.9}
\contentsline {subsection}{\numberline {9.1}Acknowledgements}{i}{subsection.9.1}
\contentsline {paragraph}{}{i}{section*.164}
\contentsline {subsection}{\numberline {9.2}Properties of TGAS downloads}{i}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Python Script to Calculate $\epsilon $}{i}{subsection.9.3}
\contentsline {paragraph}{}{i}{section*.166}
\contentsline {subsection}{\numberline {9.4}4-Stage Subclustering Script}{i}{subsection.9.4}
\contentsline {subsection}{\numberline {9.5}Subclustering Plotting Script}{ii}{subsection.9.5}
\contentsline {subsection}{\numberline {9.6}3-D Subclustering Script}{iv}{subsection.9.6}
\contentsline {subsection}{\numberline {9.7}$\sigma $ Calculation For 4-Stage Method Contours}{v}{subsection.9.7}
\contentsline {paragraph}{}{v}{section*.167}
\contentsline {section}{List of Figures}{v}{table.caption.168}
\contentsline {section}{List of Tables}{vi}{section*.169}
\contentsline {section}{References}{vi}{section*.170}
