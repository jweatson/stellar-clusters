#Stellar Clusters Project
##University of Leeds

*Author: Joseph Eatson*

*Supervisor: Dr Stuart Lumsden*

*Purpose: Final year master's project, 2016*

The main aim of this project is to devise a new algorithm for sublustering, utilising ageing metrics such as spectroscopic lithium abundances and HR fitting to better define the regions of a cluster. This will hopefully be an improvement on the previous algorithm, developed in 2015.

The young open cluster NGC 2264 is being used intially, with potentially other clusters with reliable ageing data being considered if successful with the candidate.

This is a backup of the project data, using a git sync script that also syncs to a memory stick, and an off site server at my house.
