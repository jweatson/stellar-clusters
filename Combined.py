# -*- coding: utf-8 -*-
"""
Created on Wed Feb 01 16:30:52 2017

@author: jweat
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit
data=np.genfromtxt(
                   "WFCAM-Combined.csv",
                   skip_header=1,
                   delimiter=",",)
X=data[0:,86]
Y=data[0:,88]
Z=data[0:,92]
xlab="J-K Colour (Mag)"
ylab="H-K Colour (Mag)"
zlab="Hydrogen-Alpha Line Width (Angstrom)"

plt.figure()
ax1 = plt.subplot(111,  projection='3d')

ax1.scatter(X, Y, Z, c='b', marker='.', alpha=0.4)
ax1.scatter(X, Y, c='k', marker='.', alpha=0.1)

ax1.set_xlabel(xlab)
ax1.set_ylabel(ylab)
ax1.set_zlabel(zlab)
plt.savefig("Halpha-3D.png",dpi=600)

plt.figure()
ax2 = plt.subplot(111,  projection='3d')

cx = np.ones_like(X) * ax1.get_xlim3d()[0]
cy = np.ones_like(X) * ax1.get_ylim3d()[1]
cz = np.ones_like(Z) * ax1.get_zlim3d()[0]

ax2.scatter(X, Y, cz, c="r",  marker='.', lw=0, alpha=0.4)
ax2.scatter(X, cy, Z, c="g", marker='.', lw=0, alpha=0.4)
ax2.scatter(cx, Y, Z, c="b",  marker='.', lw=0, alpha=0.4)

ax2.set_xlim3d(ax1.get_xlim3d())
ax2.set_ylim3d(ax1.get_ylim3d())
ax2.set_zlim3d(ax1.get_zlim3d())
ax2.set_xlabel(xlab)
ax2.set_ylabel(ylab)
ax2.set_zlabel(zlab)

plt.savefig("Halpha-Composite.png",dpi=600)